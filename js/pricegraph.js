//pricegraph.js
/*jshint esnext: true */
/*global d3: true*/

/*
 * The width and height of the line plot area.
 * Does not include the axes.
 */
const plot_w = 400, plot_h = 300;
const margin = {left: 60, top: 10, right: 60, bottom: 30};
// The small gap between the line plot area and the axes
const axis_padding = 3;
const num_x_ticks = 4, num_y_ticks = 4;
/*
 * Scales and axis functions
 */
let XScale, YScale;
let XAxis, YAxis;
/*
 * svg group elements that contain the line plot area,
 * the x axis, and the y axis.
 */
let x_axis_grp, y_axis_grp;
/*
 * Map that stores line graphs for all price types
 */
let graph_map = new Map();
/*
 * An array that consists of the range values
 * for the x axis ordinal scale.
 */
let x_range;
/*
 * Map of circles showing the closest x coordinate
 * to the mouse.
 */
let focus_circle_map = new Map();
/*
 * An empty svg rect for enabling
 * mouse movement over the plot area.
 */
let plot_overlay;

let tooltip_div, date_p;

function onMousemove(x_pos, data, type_map) {
  /*
   * Here we find out the two range elements
   * between which our x pos lies. Then we find out
   * the closest range value.
   */
  let range_index_left = d3.bisect(x_range, x_pos) - 1;
  let range_index_right = range_index_left + 1;
  let range_left_diff = x_pos - x_range[range_index_left];
  let range_right_diff = x_range[range_index_right] - x_pos;
  let range_index = (range_left_diff < range_right_diff) ? range_index_left : range_index_right;
  if (range_index > x_range.length - 1) {
    --range_index;
  }
  /*
   * If more than one type of graph is shown the tooltip
   * is shown beneath the graph with the lowest value.
   * cymax is that value.
   */
  let cx = x_range[range_index], cymax = 0;
  /*
   * Iterate through the map of circles and set
   * the position of those for which the graphs are shown.
   */
  focus_circle_map.forEach((v,k) => {
    if (type_map[k]) {
      let cy = YScale(+data[range_index][k]);
      v.attr({cx: cx, cy: cy});
      if ( cy > cymax ) {
        cymax = cy;
      }
    }
  });
  date_p.innerHTML = `Date: ${data[range_index].Date}`;
  Object.keys(type_map).forEach((d) => {
    if (type_map[d]) {
      let type_p = document.getElementById(d + "_tooltip_p");
      type_p.innerHTML = `${d}: ${data[range_index][d]}`;
    }
  });
  tooltip_div.style.left = (cx + margin.left + 10) + "px";
  tooltip_div.style.top = Math.floor(cymax + margin.top + 10) + "px";
}

function showTooltip() {
  tooltip_div.style.display = "block";
}

function showFocusCircles(type_map) {
  focus_circle_map.forEach((v, k) => {
    v.style("display", type_map[k] ? null : "none");
  });
}

function onMouseover(type_map) {
  showFocusCircles(type_map);
  if (Object.keys(type_map)
            .some((el) => type_map[el])) {
    showTooltip();
  }
}

function hideFocusCircles() {
  focus_circle_map.forEach((v) => {
    v.style("display", "none");
  });
}

function hideTooltip() {
  tooltip_div.style.display = "none";
}

function onMouseout() {
  hideFocusCircles();
  hideTooltip();
}

function showInTooltip(type, show) {
  let tooltip_p = document.getElementById(type + "_tooltip_p");
  tooltip_p.style.display = show ? "block" : "none";
}

function showType(type, show) {
  graph_map.get(type).style("display", show ? null : "none");
}

function toggleType(type, show) {
  showType(type, show);
  showInTooltip(type, show);
}

function graphPath(price_type, data) {
  let lineFunction = d3.svg.line()
                           .x((d) => XScale(d.Date))
                           .y((d) => YScale(+d[price_type]))
                           .interpolate("linear");
  return lineFunction(data);
}

function plotLines(data) {
  graph_map.forEach((v, k) => v.attr("d", graphPath(k, data)));
}

function plotXAxis() {
  x_axis_grp.call(XAxis);
}

function plotYAxis() {
  y_axis_grp.call(YAxis);
}

function setYScale(data, type_map) {
  let min_vals = [], max_vals = [];
  Object.keys(type_map).forEach((el) => {
    min_vals.push(d3.min(data, (d) => +d[el]));
    max_vals.push(d3.max(data, (d) => +d[el]));
  });
  let most_min_val = Math.min(...min_vals);
  let most_max_val = Math.max(...max_vals);
  let span = most_max_val - most_min_val;
  YScale.domain([most_min_val - span/8, most_max_val + span/8]);
}

function setXScale(data) {
  let dates = data.map((d) => d.Date);
  XScale.domain(dates);
  x_range = XScale.range();
  let tick_span = Math.floor(dates.length/(num_x_ticks - 1));
  let ticks = XScale.domain().filter((d,i) => i % tick_span === 0);
  XAxis.tickValues(ticks);
}

function zoomPlot(data) {
  setXScale(data);
  plotXAxis();
  plotLines(data);
}

function plot(data, type_map) {
  setXScale(data);
  setYScale(data, type_map);
  plotXAxis();
  plotYAxis();
  plotLines(data);
}

function init(type_map) {
  tooltip_div = document.getElementById("tooltip_div");
  date_p = document.getElementById("date_p");
  let total_width = plot_w + margin.left + margin.right;
  let total_height = plot_h + margin.top + margin.bottom;
  let graph_svg = d3.select("#price_graph_div")
                          .append("svg")
                          .attr({width: total_width, height: total_height});
  let plot_grp = graph_svg.append("g")
                                  .attr("transform", `translate(${margin.left}, ${margin.top})`);
  XScale = d3.scale.ordinal().rangePoints([0, plot_w]);
  YScale = d3.scale.linear().range([plot_h, 0]);
  x_axis_grp = graph_svg.append("g")
          .attr("transform", `translate(${margin.left}, ${total_height - margin.bottom + axis_padding})`)
          .attr("class", "axis");
  y_axis_grp = graph_svg.append("g")
                  .attr("transform", `translate(${margin.left - axis_padding}, ${margin.top})`)
                  .attr("class", "axis");

  XAxis = d3.svg.axis().orient("bottom").scale(XScale);
  YAxis = d3.svg.axis().orient("left").scale(YScale).ticks(num_y_ticks);
  let colorScale = d3.scale.category10();
  Object.keys(type_map).forEach((k, i) => {
    let line_graph = plot_grp.append("path")
                      .attr("class", "line")
                      .attr("stroke", colorScale(i))
                      .style("display", type_map[k] ? null: "none");
    graph_map.set(k, line_graph);
    let focus_circle = plot_grp.append("circle")
                        .attr({fill: "none", stroke: colorScale(i), r: 4.5})
                        .style("display", "none");
    focus_circle_map.set(k, focus_circle);
    d3.select(tooltip_div)
      .append("p")
      .attr("id", k + "_tooltip_p")
      .style("color", colorScale(i))
      .style("display", type_map[k] ? "block": "none");
  });
  plot_overlay = plot_grp.append("rect")
                         .attr("class", "rectoverlay")
                         .attr("width", plot_w)
                         .attr("height", plot_h);
}

let pricegraph = {
  get overlay() {
    return plot_overlay;
  },
  init: init,
  onMouseover: onMouseover,
  onMouseout: onMouseout,
  onMousemove: onMousemove,
  toggleType: toggleType,
  zoomPlot: zoomPlot,
  plot: plot
};

export default pricegraph;
