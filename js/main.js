//main.js
/*jshint esnext: true */
/*global d3: true*/

import pricegraph from "./pricegraph";

import {zoomed_data, onZoom} from "./zoompan";
/*
 * Historical stock market data retrieved from yahoo finance.
 * 'data' is an array of quotes of a particular symbol between two dates.
 * A quote is an object with 'Symbol', 'Date', 'Low', 'High',
 * etc as keys.
 */
let data;
/*
 * To add mouse event listeners
 * for the first plot.
 */
let first_click = true;

/*
 * This map decides the graphs to be shown.
 * Initially only the 'Close' type is shown.
 */
let price_type_map = {"Open": false, "High": false,
  "Low": false, "Close": true, "Adj_Close": false};

function loadData(url) {
  return new Promise((resolve, reject) => {
    d3.json(url, function(err, d) {
      if (err) {
        console.log("Error:", err);
        reject(err);
        return;
      }
      resolve(d);
    });
  });
}

function getUrl(symbol, start_date, end_date){
  return `http://query.yahooapis.com/v1/public/yql?q=select * from yahoo.finance.historicaldata where symbol = \"${symbol}\" and startDate = \"${start_date}\" and endDate = \"${end_date}\"&format=json&env=store://datatables.org/alltableswithkeys`;
}

function onPlotBtnClick(){
  let symbol = document.getElementById("symbol_input").value;
  let start_date = document.getElementById("start_date_input").value;
  let end_date = document.getElementById("end_date_input").value;
  if ( symbol.trim() !== "" && start_date.trim() !== "" &&
      end_date.trim() !== "" ) {
    let url = encodeURI(getUrl(symbol, start_date, end_date));
    loadData(url).then((d) => {
      let quotes = d.query.results.quote;
      /*
       * The dates are received from end date to start date.
       * So we reverse the array to have the dates in increasing order.
       */
      data = quotes.map((el, i) => quotes[quotes.length - i - 1]);
      pricegraph.plot(data, price_type_map);
      if ( first_click ) {
        first_click = false;
        let cbs_p = document.getElementById("price_type_cbs_p");
        let cbs = cbs_p.querySelectorAll("input");
        // Event listeners for the checkboxes
        cbs.forEach((cb) => cb.addEventListener("change", (e) => {
          let id = e.target.id;
          let price_type = id.replace("_price_cb", "");
          price_type_map[price_type] = e.target.checked;
          pricegraph.toggleType(price_type, price_type_map[price_type]);
        }));
        //zoom and pan are both handled by the same behavior
        let zoomBehavior = d3.behavior.zoom()
          .on("zoomstart", () => d3.event.sourceEvent.stopPropagation())
          .on("zoom", () => {
            let scale = d3.event.scale;
            let trans = d3.event.translate[0];
            onZoom(data, scale, trans);
          });
        pricegraph.overlay.call(zoomBehavior);
        pricegraph.overlay
          .on("mouseover", () => {
            pricegraph.onMouseover(price_type_map);})
          .on("mouseout", () => {
            pricegraph.onMouseout();})
          .on("mousemove", function() {
            /*
             * Here 'this' is the svg element that generates
             * the mouse events. We use the x position([0]) only.
             */
            let x_pos = d3.mouse(this)[0];
            let cur_data = zoomed_data ? zoomed_data : data;
            pricegraph.onMousemove(x_pos, cur_data, price_type_map);
          });
      }
    }).catch((err) => {
      let query_err_p = document.getElementById("query_err_p");
      query_err_p.innerHTML = "Error fetching data";
    });
  }
}

function init(){
  let price_types = Object.keys(price_type_map);
  let colorScale = d3.scale.category10();
  // creating the checkboxes
  d3.select("#price_type_cbs_p")
    .selectAll("label")
    .data(price_types)
    .enter()
    .append("label")
    .style("color", (d, i) => colorScale(i))
    .text((d) => d)
    .append("input")
    .attr("type", "checkbox")
    .property("checked", (d) => price_type_map[d])
    .attr("id", (d) => d + "_price_cb");

  pricegraph.init(price_type_map);

  let plot_btn = document.getElementById("plot_btn");
  plot_btn.addEventListener("click", onPlotBtnClick);
}

init();
