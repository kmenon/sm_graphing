//zoompan.js
/*jshint esnext: true */

import pricegraph from "./pricegraph";

/*
 * zoom_limits is an array of objects each of which stores two properties:
 * left and right. The left property is the array index of the starting point
 * of the current zoomed-in data( actually one less than the starting point)
 * w.r.t the full data.
 * Similarly the right property is the array index of the end point( plus one )
 * of the current zoomed-in data.
 * Every zoom-in produces a new object. And on zoom-out these objects are popped
 * out.
 * Panning to the left subtracts the pan amount from all the left, right
 * properties of all the objects. And panning to the right adds the pan amount
 * to all the left, right properties of all the objects
 */
let zoom_limits = [];
let cur_scale = 1, prev_scale = 1;
let cur_trans = 0, prev_trans = 0;

export let zoomed_data;

function getCurDataLimits(data) {
  let cur_data_start = 0, cur_data_end = data.length - 1;
  if (zoom_limits.length !== 0) {
    let zl_last = zoom_limits[zoom_limits.length - 1];
    let left = zl_last.left;
    let right = zl_last.right;
    if (left < -1) {
      right += (-1 - left);
    } else if ( right > data.length ) {
      left -= (right - data.length);
    }
    cur_data_start = Math.max(0, left + 1);
    cur_data_end = Math.min(data.length - 1, right - 1);
  }
  return [cur_data_start, cur_data_end];
}

/*
 * On every zooming-in 1/10th of the current length is removed
 * from either side of the currently shown data.
 */
function trimDomain(data) {
  let [cur_data_start, cur_data_end] = getCurDataLimits(data);
  let cur_data_len = cur_data_end - cur_data_start + 1;
  let trim_amt = Math.floor(cur_data_len/10);
  if (trim_amt !== 0) {
    let limit_obj = {};
    limit_obj.left = cur_data_start + trim_amt - 1;
    limit_obj.right = cur_data_end - trim_amt + 1;
    zoom_limits.push(limit_obj);
    return true;
  }
  return false;
}

function expandDomain() {
  if (zoom_limits.length !== 0) {
    zoom_limits.pop();
    return true;
  }
  return false;
}

function onPan(drag_amt, data) {
  if (zoom_limits.length === 0) {
    return false;
  }
  let zl_last = zoom_limits[zoom_limits.length - 1];
  if (((drag_amt < 0) && (zl_last.left + drag_amt < -1)) ||
    ((drag_amt > 0) && (zl_last.right + drag_amt > data.length))) {
    return false;
  }
  zoom_limits.forEach((zl) => {
    zl.left += drag_amt;
    zl.right += drag_amt;
  });
  return true;
}

export function onZoom(data, scale, trans) {
  cur_scale = scale;
  let rel_scale_diff = (cur_scale - prev_scale)/prev_scale;
  prev_scale = cur_scale;

  cur_trans = trans;
  let trans_diff = cur_trans - prev_trans;
  prev_trans = cur_trans;

  let redraw = false;
  if (Math.abs(rel_scale_diff) < 0.001) {
    if (trans_diff !== 0) {
      trans_diff = trans_diff > 0 ? -1: 1;
      //redraw = onPan(-trans_diff, data);
      redraw = onPan(trans_diff, data);
    }
  } else if (rel_scale_diff > 0.0) {
    redraw = trimDomain(data);
  } else if (rel_scale_diff < 0.0) {
    redraw = expandDomain();
  }

  if (redraw) {
    let [cur_data_start, cur_data_end] = getCurDataLimits(data);
    zoomed_data = data.slice(cur_data_start, cur_data_end + 1);
    pricegraph.zoomPlot(zoomed_data);
  }
}

