//pricegraph.js
/*jshint esnext: true */
/*global d3: true*/

/*
 * The width and height of the line plot area.
 * Does not include the axes.
 */
const plot_w = 400;
const plot_h = 300;
const margin = {left: 60, top: 10, right: 60, bottom: 30};
// The small gap between the line plot area and the axes
const axis_padding = 3;
const num_x_ticks = 4;
const num_y_ticks = 4;
/*
 * Scales and axis functions
 */
let XScale;
let YScale;
let XAxis;
let YAxis;
/*
 * svg group elements that contain the line plot area,
 * the x axis, and the y axis.
 */
let x_axis_grp;
let y_axis_grp;
/*
 * Map that stores line graphs for all price types
 */
let graph_map = new Map();
/*
 * An array that consists of the range values
 * for the x axis ordinal scale.
 */
let x_range;
/*
 * Map of circles showing the closest x coordinate
 * to the mouse.
 */
let focus_circle_map = new Map();
/*
 * An empty svg rect for enabling
 * mouse movement over the plot area.
 */
let plot_overlay;

let tooltip_div;
let date_p;

function onMousemove(x_pos, data, type_map) {
  /*
   * Here we find out the two range elements
   * between which our x pos lies. Then we find out
   * the closest range value.
   */
  let range_index_left = d3.bisect(x_range, x_pos) - 1;
  let range_index_right = range_index_left + 1;
  let range_left_diff = x_pos - x_range[range_index_left];
  let range_right_diff = x_range[range_index_right] - x_pos;
  let range_index = (range_left_diff < range_right_diff) ? range_index_left : range_index_right;
  if (range_index > x_range.length - 1) {
    --range_index;
  }
  /*
   * If more than one type of graph is shown the tooltip
   * is shown beneath the graph with the lowest value.
   * cymax is that value.
   */
  let cx = x_range[range_index], cymax = 0;
  /*
   * Iterate through the map of circles and set
   * the position of those for which the graphs are shown.
   */
  focus_circle_map.forEach((v,k) => {
    if (type_map[k]) {
      let cy = YScale(+data[range_index][k]);
      v.attr({cx: cx, cy: cy});
      if ( cy > cymax ) {
        cymax = cy;
      }
    }
  });
  date_p.innerHTML = `Date: ${data[range_index].Date}`;
  Object.keys(type_map).forEach((d) => {
    if (type_map[d]) {
      let type_p = document.getElementById(d + "_tooltip_p");
      type_p.innerHTML = `${d}: ${data[range_index][d]}`;
    }
  });
  tooltip_div.style.left = (cx + margin.left + 10) + "px";
  tooltip_div.style.top = Math.floor(cymax + margin.top + 10) + "px";
}

function showTooltip() {
  tooltip_div.style.display = "block";
}

function showFocusCircles(type_map) {
  focus_circle_map.forEach((v, k) => {
    v.style("display", type_map[k] ? null : "none");
  });
}

function onMouseover(type_map) {
  showFocusCircles(type_map);
  if (Object.keys(type_map)
            .some((el) => type_map[el])) {
    showTooltip();
  }
}

function hideFocusCircles() {
  focus_circle_map.forEach((v) => {
    v.style("display", "none");
  });
}

function hideTooltip() {
  tooltip_div.style.display = "none";
}

function onMouseout() {
  hideFocusCircles();
  hideTooltip();
}

function showInTooltip(type, show) {
  let tooltip_p = document.getElementById(type + "_tooltip_p");
  tooltip_p.style.display = show ? "block" : "none";
}

function showType(type, show) {
  graph_map.get(type).style("display", show ? null : "none");
}

function toggleType(type, show) {
  showType(type, show);
  showInTooltip(type, show);
}

function graphPath(price_type, data) {
  let lineFunction = d3.svg.line()
                           .x((d) => XScale(d.Date))
                           .y((d) => YScale(+d[price_type]))
                           .interpolate("linear");
  return lineFunction(data);
}

function plotLines(data) {
  graph_map.forEach((v, k) => v.attr("d", graphPath(k, data)));
}

function plotXAxis() {
  x_axis_grp.call(XAxis);
}

function plotYAxis() {
  y_axis_grp.call(YAxis);
}

function setYScale(data, type_map) {
  let min_vals = [], max_vals = [];
  Object.keys(type_map).forEach((el) => {
    min_vals.push(d3.min(data, (d) => +d[el]));
    max_vals.push(d3.max(data, (d) => +d[el]));
  });
  let most_min_val = Math.min(...min_vals);
  let most_max_val = Math.max(...max_vals);
  let span = most_max_val - most_min_val;
  YScale.domain([most_min_val - span/8, most_max_val + span/8]);
}

function setXScale(data) {
  let dates = data.map((d) => d.Date);
  XScale.domain(dates);
  x_range = XScale.range();
  let tick_span = Math.floor(dates.length/(num_x_ticks - 1));
  let ticks = XScale.domain().filter((d,i) => i % tick_span === 0);
  XAxis.tickValues(ticks);
}

function zoomPlot(data) {
  setXScale(data);
  plotXAxis();
  plotLines(data);
}

function plot(data, type_map) {
  setXScale(data);
  setYScale(data, type_map);
  plotXAxis();
  plotYAxis();
  plotLines(data);
}

function init$1(type_map) {
  tooltip_div = document.getElementById("tooltip_div");
  date_p = document.getElementById("date_p");
  let total_width = plot_w + margin.left + margin.right;
  let total_height = plot_h + margin.top + margin.bottom;
  let graph_svg = d3.select("#price_graph_div")
                          .append("svg")
                          .attr({width: total_width, height: total_height});
  let plot_grp = graph_svg.append("g")
                                  .attr("transform", `translate(${margin.left}, ${margin.top})`);
  XScale = d3.scale.ordinal().rangePoints([0, plot_w]);
  YScale = d3.scale.linear().range([plot_h, 0]);
  x_axis_grp = graph_svg.append("g")
          .attr("transform", `translate(${margin.left}, ${total_height - margin.bottom + axis_padding})`)
          .attr("class", "axis");
  y_axis_grp = graph_svg.append("g")
                  .attr("transform", `translate(${margin.left - axis_padding}, ${margin.top})`)
                  .attr("class", "axis");

  XAxis = d3.svg.axis().orient("bottom").scale(XScale);
  YAxis = d3.svg.axis().orient("left").scale(YScale).ticks(num_y_ticks);
  let colorScale = d3.scale.category10();
  Object.keys(type_map).forEach((k, i) => {
    let line_graph = plot_grp.append("path")
                      .attr("class", "line")
                      .attr("stroke", colorScale(i))
                      .style("display", type_map[k] ? null: "none");
    graph_map.set(k, line_graph);
    let focus_circle = plot_grp.append("circle")
                        .attr({fill: "none", stroke: colorScale(i), r: 4.5})
                        .style("display", "none");
    focus_circle_map.set(k, focus_circle);
    d3.select(tooltip_div)
      .append("p")
      .attr("id", k + "_tooltip_p")
      .style("color", colorScale(i))
      .style("display", type_map[k] ? "block": "none");
  });
  plot_overlay = plot_grp.append("rect")
                         .attr("class", "rectoverlay")
                         .attr("width", plot_w)
                         .attr("height", plot_h);
}

let pricegraph = {
  get overlay() {
    return plot_overlay;
  },
  init: init$1,
  onMouseover: onMouseover,
  onMouseout: onMouseout,
  onMousemove: onMousemove,
  toggleType: toggleType,
  zoomPlot: zoomPlot,
  plot: plot
};

//zoompan.js
/*jshint esnext: true */

/*
 * zoom_limits is an array of objects each of which stores two properties:
 * left and right. The left property is the array index of the starting point
 * of the current zoomed-in data( actually one less than the starting point)
 * w.r.t the full data.
 * Similarly the right property is the array index of the end point( plus one )
 * of the current zoomed-in data.
 * Every zoom-in produces a new object. And on zoom-out these objects are popped
 * out.
 * Panning to the left subtracts the pan amount from all the left, right
 * properties of all the objects. And panning to the right adds the pan amount
 * to all the left, right properties of all the objects
 */
let zoom_limits = [];
let cur_scale = 1;
let prev_scale = 1;
let cur_trans = 0;
let prev_trans = 0;

let zoomed_data;

function getCurDataLimits(data) {
  let cur_data_start = 0, cur_data_end = data.length - 1;
  if (zoom_limits.length !== 0) {
    let zl_last = zoom_limits[zoom_limits.length - 1];
    let left = zl_last.left;
    let right = zl_last.right;
    if (left < -1) {
      right += (-1 - left);
    } else if ( right > data.length ) {
      left -= (right - data.length);
    }
    cur_data_start = Math.max(0, left + 1);
    cur_data_end = Math.min(data.length - 1, right - 1);
  }
  return [cur_data_start, cur_data_end];
}

/*
 * On every zooming-in 1/10th of the current length is removed
 * from either side of the currently shown data.
 */
function trimDomain(data) {
  let [cur_data_start, cur_data_end] = getCurDataLimits(data);
  let cur_data_len = cur_data_end - cur_data_start + 1;
  let trim_amt = Math.floor(cur_data_len/10);
  if (trim_amt !== 0) {
    let limit_obj = {};
    limit_obj.left = cur_data_start + trim_amt - 1;
    limit_obj.right = cur_data_end - trim_amt + 1;
    zoom_limits.push(limit_obj);
    return true;
  }
  return false;
}

function expandDomain() {
  if (zoom_limits.length !== 0) {
    zoom_limits.pop();
    return true;
  }
  return false;
}

function onPan(drag_amt, data) {
  if (zoom_limits.length === 0) {
    return false;
  }
  let zl_last = zoom_limits[zoom_limits.length - 1];
  if (((drag_amt < 0) && (zl_last.left + drag_amt < -1)) ||
    ((drag_amt > 0) && (zl_last.right + drag_amt > data.length))) {
    return false;
  }
  zoom_limits.forEach((zl) => {
    zl.left += drag_amt;
    zl.right += drag_amt;
  });
  return true;
}

function onZoom(data, scale, trans) {
  cur_scale = scale;
  let rel_scale_diff = (cur_scale - prev_scale)/prev_scale;
  prev_scale = cur_scale;

  cur_trans = trans;
  let trans_diff = cur_trans - prev_trans;
  prev_trans = cur_trans;

  let redraw = false;
  if (Math.abs(rel_scale_diff) < 0.001) {
    if (trans_diff !== 0) {
      trans_diff = trans_diff > 0 ? -1: 1;
      //redraw = onPan(-trans_diff, data);
      redraw = onPan(trans_diff, data);
    }
  } else if (rel_scale_diff > 0.0) {
    redraw = trimDomain(data);
  } else if (rel_scale_diff < 0.0) {
    redraw = expandDomain();
  }

  if (redraw) {
    let [cur_data_start, cur_data_end] = getCurDataLimits(data);
    zoomed_data = data.slice(cur_data_start, cur_data_end + 1);
    pricegraph.zoomPlot(zoomed_data);
  }
}

//main.js
/*jshint esnext: true */
/*global d3: true*/

/*
 * Historical stock market data retrieved from yahoo finance.
 * 'data' is an array of quotes of a particular symbol between two dates.
 * A quote is an object with 'Symbol', 'Date', 'Low', 'High',
 * etc as keys.
 */
let data;
/*
 * To add mouse event listeners
 * for the first plot.
 */
let first_click = true;

/*
 * This map decides the graphs to be shown.
 * Initially only the 'Close' type is shown.
 */
let price_type_map = {"Open": false, "High": false,
  "Low": false, "Close": true, "Adj_Close": false};

function loadData(url) {
  return new Promise((resolve, reject) => {
    d3.json(url, function(err, d) {
      if (err) {
        console.log("Error:", err);
        reject(err);
        return;
      }
      resolve(d);
    });
  });
}

function getUrl(symbol, start_date, end_date){
  return `http://query.yahooapis.com/v1/public/yql?q=select * from yahoo.finance.historicaldata where symbol = \"${symbol}\" and startDate = \"${start_date}\" and endDate = \"${end_date}\"&format=json&env=store://datatables.org/alltableswithkeys`;
}

function onPlotBtnClick(){
  let symbol = document.getElementById("symbol_input").value;
  let start_date = document.getElementById("start_date_input").value;
  let end_date = document.getElementById("end_date_input").value;
  if ( symbol.trim() !== "" && start_date.trim() !== "" &&
      end_date.trim() !== "" ) {
    let url = encodeURI(getUrl(symbol, start_date, end_date));
    loadData(url).then((d) => {
      let quotes = d.query.results.quote;
      /*
       * The dates are received from end date to start date.
       * So we reverse the array to have the dates in increasing order.
       */
      data = quotes.map((el, i) => quotes[quotes.length - i - 1]);
      pricegraph.plot(data, price_type_map);
      if ( first_click ) {
        first_click = false;
        let cbs_p = document.getElementById("price_type_cbs_p");
        let cbs = cbs_p.querySelectorAll("input");
        // Event listeners for the checkboxes
        cbs.forEach((cb) => cb.addEventListener("change", (e) => {
          let id = e.target.id;
          let price_type = id.replace("_price_cb", "");
          price_type_map[price_type] = e.target.checked;
          pricegraph.toggleType(price_type, price_type_map[price_type]);
        }));
        //zoom and pan are both handled by the same behavior
        let zoomBehavior = d3.behavior.zoom()
          .on("zoomstart", () => d3.event.sourceEvent.stopPropagation())
          .on("zoom", () => {
            let scale = d3.event.scale;
            let trans = d3.event.translate[0];
            onZoom(data, scale, trans);
          });
        pricegraph.overlay.call(zoomBehavior);
        pricegraph.overlay
          .on("mouseover", () => {
            pricegraph.onMouseover(price_type_map);})
          .on("mouseout", () => {
            pricegraph.onMouseout();})
          .on("mousemove", function() {
            /*
             * Here 'this' is the svg element that generates
             * the mouse events. We use the x position([0]) only.
             */
            let x_pos = d3.mouse(this)[0];
            let cur_data = zoomed_data ? zoomed_data : data;
            pricegraph.onMousemove(x_pos, cur_data, price_type_map);
          });
      }
    }).catch((err) => {
      let query_err_p = document.getElementById("query_err_p");
      query_err_p.innerHTML = "Error fetching data";
    });
  }
}

function init(){
  let price_types = Object.keys(price_type_map);
  let colorScale = d3.scale.category10();
  // creating the checkboxes
  d3.select("#price_type_cbs_p")
    .selectAll("label")
    .data(price_types)
    .enter()
    .append("label")
    .style("color", (d, i) => colorScale(i))
    .text((d) => d)
    .append("input")
    .attr("type", "checkbox")
    .property("checked", (d) => price_type_map[d])
    .attr("id", (d) => d + "_price_cb");

  pricegraph.init(price_type_map);

  let plot_btn = document.getElementById("plot_btn");
  plot_btn.addEventListener("click", onPlotBtnClick);
}

init();
